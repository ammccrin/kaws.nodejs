/**
 * Created by cj on 5/13/18.
 */
'use strict';

const svc = require('../app/services/orderService');

describe('Testing order service', () => {
    it('Should track order', done => {
        svc.trackOrder({
            id: 214083, 
            email: 'chraznguyen@gmail.com'
        }).then(data => {
            console.log(data);
            done();
        }).catch(err => done(err));
    })
})
