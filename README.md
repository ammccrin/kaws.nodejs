# kaws.nodejs

## Installation
1. Download and install [Node.Js](https://nodejs.org/en/)  
Version requirements>= v6.11.5

2. Fork and clone the repo

3. Navigate to the app folder using terminal app.
   For windows you can use [GitBash](https://git-scm.com/downloads), or Terminal / iTerm for Mac OSX.
    
4. Install the included packages: 
  `npm install --dev`
  
  _I recommend installing gulp, mocha and nodemon globally: `npm install -g nodemon gulp mocha`_
  
5. run the app using gulp: `gulp`


## Folders Structure
Folder  | Content
------------- | -------------
/app  | Server side code
/src  | Client side code
/src/js/app  | Client side javascript - custom source files
/src/js/vendor | Client side javascript - 3rd party files to include
/src/scss  | Client side *sass* - Source Files
/views | HTML files (using *pug* template engine)
/public/js | \* bundled javascript files from /src/js
/public/css | \* bundled css files compiled from sass folder (/src/scss)
/public/img | Image assets folder

\* DO NOT SAVE FILES MANUALLY INTO /public/js or /public/css

#### Helpful links
[pug documentation](https://pugjs.org/api/getting-started.html)

[HMTL to pug (formerly jade)](http://html2jade.org/)

[sass documentation](https://sass-lang.com/documentation/file.SASS_REFERENCE.html)


#### Common Gulp Tasks
Code | Task
---- | ----
`gulp app:js` | bundle custom JS files and output into /public/js/app.js
`gulp app:style` | compile sass files from /src/scss and output into /public/css/main.css
`gulp app:vendor:js` | bundle vendor js files and output to /public/js/vendor.js
`gulp app:vendor:css` | bundle css files from 3rd party vendors and output into /public/css/vendor.css


#### Developer common tasks
Creating new **view** (HTML) file:
> Easiest way to get started is to create your HTML and then convert it using [HTML2Jade](http://html2jade.org/)

Working on CSS:
> add .scss file in /src/scss/base choose a name that represent the component you are working on. Import that file into /src/scss/main.scss

Custom JS:
> I copied all the on-page js into /src/app/kaws.main.js. I recommend adding custom scripts to this file.

Registering JS framework (e.g. jQuery, lightbox, remodal, etc...):
> Preferred method is to include the package using **npm**:
>> 1. find the package in [npmjs](https://www.npmjs.com/)
>> 2. install the package, example: `npm install --save remodal`
>> 3. reference the js in `/Gulpfile.js` by adding it to the array: `sources.vendor.js`
>> 4. reference css in `/Gulpfile.js` by adding it to the array: `sources.vendor.css`
>> 5. recompile vendor css and js : 
       ```
        gulp app:vendor:js
        gulp app:vendor:css
       ```