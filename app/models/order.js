/**
 * Created by cj on 5/13/18.
 */
'use strict';

/**
 * @todo: consider breaking down the schema into more streamline tables.
 DB Schema:
 
 CREATE TABLE public."order" (
 id SERIAL,
 active SMALLINT DEFAULT 1,
 insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now() NOT NULL,
 bill_first_name VARCHAR,
 bill_last_name VARCHAR,
 bill_address VARCHAR,
 bill_address2 VARCHAR,
 bill_city VARCHAR,
 bill_state VARCHAR,
 bill_country VARCHAR,
 bill_zip VARCHAR,
 ship_first_name VARCHAR,
 ship_last_name VARCHAR,
 ship_address VARCHAR,
 ship_address2 VARCHAR,
 ship_city VARCHAR,
 ship_state VARCHAR,
 ship_country VARCHAR,
 ship_zip VARCHAR,
 phone VARCHAR,
 tax VARCHAR,
 tax_rate VARCHAR,
 status VARCHAR,
 tracking_number VARCHAR,
 shipping_method VARCHAR,
 user_id INTEGER,
 coupon_code VARCHAR,
 gift_card VARCHAR,
 email VARCHAR,
 shipping_cost REAL,
 payment_method INTEGER,
 subtotal REAL,
 total REAL,
 coupon_type VARCHAR,
 coupon_amount REAL,
 viewed BOOLEAN,
 comment VARCHAR,
 fraud_status VARCHAR,
 cc_number NUMERIC(16,0),
 cc_expiration_month INTEGER,
 cc_expiration_year INTEGER,
 cc_ccv INTEGER,
 note VARCHAR,
 guest_id VARCHAR,
 card_name VARCHAR,
 error_message VARCHAR,
 ref_num VARCHAR,
 in_store SMALLINT DEFAULT 0,
 paypal_id VARCHAR,
 gift_card_amount REAL,
 newsletter_discount NUMERIC(10,2),
 payment_status VARCHAR,
 email_log TEXT,
 trans_id VARCHAR,
 discount NUMERIC(10,2),
 payment_id VARCHAR,
 ship_id INTEGER,
 ship_status VARCHAR,
 ship_message VARCHAR,
 wholesale_id INTEGER,
 wholesale_payment_type SMALLINT DEFAULT 0,
 ticket_id INTEGER DEFAULT 0,
 auth_number VARCHAR,
 wholesale_term INTEGER,
 additional_charge DOUBLE PRECISION DEFAULT 0,
 is_preorder INTEGER DEFAULT 0,
 case_id INTEGER,
 received_payment_method INTEGER DEFAULT 0,
 shipping_charge NUMERIC(10,2) DEFAULT 0,
 allow_returns SMALLINT DEFAULT 0,
 fulfillment_status VARCHAR,
 user_ip VARCHAR,
 entry_id INTEGER,
 campaign_id INTEGER,
 place_id VARCHAR,
 duty NUMERIC(10,2) DEFAULT 0,
 orig_total NUMERIC(10,2) DEFAULT 0,
 CONSTRAINT order_pkey PRIMARY KEY(id)
 )
 WITH (oids = false);

 CREATE INDEX idx_search ON public."order"
 USING btree (ship_first_name COLLATE pg_catalog."default", ship_last_name COLLATE pg_catalog."default", bill_first_name COLLATE pg_catalog."default", bill_last_name COLLATE pg_catalog."default");
 */


const Sequelize = require('sequelize');
const db = require('../lib/db');

const Model = module.exports = db.sequelize.define('order', {
    insert_time     : Sequelize.DATE ,
    bill_first_name : Sequelize.STRING,
    bill_last_name  : Sequelize.STRING,
    bill_address    : Sequelize.STRING,
    bill_address2   : Sequelize.STRING,
    bill_city       : Sequelize.STRING,
    bill_state      : Sequelize.STRING,
    bill_country    : Sequelize.STRING,
    bill_zip        : Sequelize.STRING,
    ship_first_name : Sequelize.STRING,
    ship_last_name  : Sequelize.STRING,
    ship_address    : Sequelize.STRING,
    ship_address2   : Sequelize.STRING,
    ship_city       : Sequelize.STRING,
    ship_state      : Sequelize.STRING,
    ship_country    : Sequelize.STRING,
    ship_zip        : Sequelize.STRING,
    phone           : Sequelize.STRING,
    tax             : Sequelize.STRING,
    tax_rate        : Sequelize.STRING,
    status          : Sequelize.STRING,
    tracking_number : Sequelize.STRING,
}, {
    // @todo : Change to `true` after adding timestamp field for soft delete (deletedAt)  
    paranoid: false,

    freezeTableName: true,


    /*
     @todo : enable after adding timestamp fields (createdAt, updatedAt) 
     */
    timestamps: false,
});

