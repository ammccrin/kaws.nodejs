/**
 * Created by cj on 5/13/18.
 */
'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const errors  = require('../lib/errors');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;


const Order = require('../models/order');
const config = require('../config');

const svc = module.exports = {};


/**
 * Returns order object for order tracking.
 * @param params
 * @return {*}
 */
svc.trackOrder = function(params) {
    if(!params.id || !params.email) {
        errors.requiredField('Order number and email are both required', 'id');
    }
    
    return Order.findOne({
        where: _.pick(params, ['id','email']),
        attributes: [
            'id',
            'ship_first_name',
            'ship_last_name',
            'ship_address',
            'ship_address2',
            'ship_city',
            'ship_state',
            'ship_country',
            'ship_zip',
            'phone',
            'status',
            'tracking_number',
            'insert_time',
        ]
    });
}

