/**
 * Created by cj on 3/11/18.
 */
'use strict'; 

const pkg = require('../../package.json'); 
const svc = require('../services/lotteryService');
const orderSvc = require('../services/orderService');
const merge = require('deepmerge');
const router = module.exports = require('express').Router(); 

router.get('/health-check', (req,res,next) => {
    return res.status(200).json({
        version: pkg.version,
        env : process.env.NODE_ENV,
        timestamp: new Date(),
    })    
});


router.post('/lottery-entry', (req,res,next) => {
    svc.createEntry(merge(req.body, {
        ip_address: req.ip_address
    })).then(data => res.status(200).json(data)).catch(err => next(err)); 
});


router.post('/order/track', (req,res,next) => {
    orderSvc.trackOrder(merge(req.body, {
        ip_address: req.ip_address
    })).then(data => res.status(200).json(data)).catch(err => next(err));
})