/**
 * Created by cj on 5/13/18.
 */
'use strict';

const router = module.exports = require('express').Router();

const svc = require('../services/orderService');

router.post('/order', (req,res,next) => {
    svc.trackOrder({
        id: req.body.ref_num,
        email: req.body.email
    }).then(data => {
        res.render('track-order', {
            data
        });
    }).catch(err => {
        res.render('track-order', {
            data: null
        });
    });
});

