'use strict'; 

const router = module.exports = require('express').Router();
const functions = require('../functions');



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render(req.state == 'pre' ? 'index-old' : 'index', { title: 'Express' });
});


/**
 * Product propfile page route
 */
router.get('/products/:slug', (req,res,next) => {
  //@todo Make product dynamic
  // res.render('product', {campaign_id: 2, product_id: 3, functions: functions});
  res.render('product', {product_id: 104, functions: functions});
});

router.get('/terms', (req,res,next) => {
  res.render('terms', {}); 
});

router.get('/payment', (req,res,next) => {
  res.render('payment', {}); 
});

router.get('/confirmation', (req,res,next) => {
  res.render('confirmation', {}); 
});

