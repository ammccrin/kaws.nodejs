/**
 * Created by cj on 4/24/18.
 */
'use strict';
const _ = require('lodash');
const merge = require('deepmerge');
const config = require('../config');
const Promise = require('bluebird');
// import SDK
const AWS = require('aws-sdk');

AWS.config.update(config.aws);

const svc = module.exports = {};
const docClient = new AWS.DynamoDB.DocumentClient();


const table_name = 'kaws-lottery'; 




/**
 * Generate a key that will be unique for an ipaddress for a specific campaign
 * 
 * @param campaign_id
 * @param ip_address
 * @return {string}
 */
svc.getKey = function({campaign_id, ip_address}) {
    if (!campaign_id || !ip_address){
        throw new Error('campaign_id and ip_address are both required!');
    }   
    
    return `${campaign_id}-${ip_address}`;
}


/**
 * Record ip address + campaign id in Dynamo DB to prevent re-submission.
 * @param params
 */
svc.recordIpAddress = function(params) {
    const putDoc = Promise.promisify(docClient.put);
    
    const key = svc.getKey(params);
    const data = {
        TableName :table_name,
        Item: merge(params, {
            id: key, 
            // add extra fields here
        })
    };

    return new Promise((resolve, reject) => {
        
        docClient.put(data, (err, data) => {
            if(err){
                return reject(err);
            } else {
                return resolve(data);
            }
        })    
    })
}

/**
 * Check the ip table and return ip entries for the current campaign
 * @param params
 */
svc.checkIpAddress = function(params) {
    const key = svc.getKey(params);
    
    const data = {
        TableName : table_name,
        KeyConditionExpression: "#id = :id",
        ExpressionAttributeNames:{
            "#id": "id"
        },
        ExpressionAttributeValues: {
            ":id":key 
        }
    };

    return new Promise((resolve, reject) => {

        docClient.query(data, (err, data) => {
            if(err){
                return reject(err);
            } else {
                return resolve(data);
            }
        })
    })
}


/**
 * returns true / false to whether the ip address already exists in DB for current campaign
 * @param params
 */
svc.ipAlreadySubmitted = function(params){
    return svc.checkIpAddress(params).then(data => {
        return data.Count > 0;
    }).catch(err => {
        // an error has occurred, we don't want to prevent user from participating in such case, so we will do nothing
        
        console.log('err ', err);
        // do nothing
        
        return false;
    })
}

svc.clearIpAddress = function(params) {
    const key = svc.getKey(params);

    const data = {
        TableName : table_name,
        Key:{
            "id":key,
        },
    };

    return new Promise((resolve, reject) => {

        docClient.delete(data, (err, data) => {
            if(err){
                return reject(err);
            } else {
                return resolve(data);
            }
        })
    })
}