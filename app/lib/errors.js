/**
 * Created by cj on 3/11/18.
 */
/**
 * 
 * @type {{errorWithCode: module.exports.errorWithCode}}
 * 
 * Error codes:
 * 
 * 0xx - generic form errors
 *  10 - missing required field. 
 * 1xx - campaign errors 
 * 2xx - user errors 
 * 3xx - lottery errors 
 * 
 * 
 */
const svc = module.exports = {
    
    errorWithCode: function(code, message, params) {
        let err = new Error(message); 
        err.code = code ;
        
        // ensure HTTP status code 400 as per RESTful best practices. 
        err.statusCode = 400;
        err.validationError = true; 
        
        Object.assign(err, params || {}); 
        
        throw err; 
    },

    termsOfService: function(){
        return svc.errorWithCode(10, 'You must agree to the terms of service')
    },

    blacklisted: function(){
        return svc.errorWithCode(210, 'There was an error processing your entry');
    },

    noActiveCampaign : function() {
        return svc.errorWithCode(100, 'This campaign has ended, entries are no longer being accepted')
    },

    productNotInCampaign : function() {
        return svc.errorWithCode(105, 'The selected product does not belong to the active campaign')
    },

    duplicateLottoEntry: function(){
        return svc.errorWithCode(110, 'Duplicate entry')
    },
    
    requiredField: function(message, field) {
        return svc.errorWithCode(10, message, {
            field 
        })
    }
    
};



